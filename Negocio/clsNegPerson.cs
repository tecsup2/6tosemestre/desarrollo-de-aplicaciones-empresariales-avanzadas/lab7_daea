﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Datos;

namespace Negocio
{
    public class clsNegPerson
    {
        clsDAOPerson daoPerson = new clsDAOPerson();

        public DataTable GetAll()
        {
            return daoPerson.GetAll();
        }

        public DataTable GetPersonById(String codigo)
        {
            return daoPerson.GetPersonById(codigo);
        }

        public DataTable GetPersonByName(String name)
        {
            return daoPerson.GetPersonByName(name);
        }

        public DataTable GetPersonByLastName(String lastname)
        {
            return daoPerson.GetPersonByLastName(lastname);
        }
    }
}
