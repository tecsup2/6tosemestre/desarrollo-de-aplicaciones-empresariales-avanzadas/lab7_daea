﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class Form1 : Form
    {

        clsNegPerson np = new clsNegPerson();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = np.GetAll();

            dgDatos.DataSource = dt;
            dgDatos.Refresh();

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            String codigo = txtPersonID.Text;


            DataTable dt = new DataTable();
            dt = np.GetPersonById(codigo);
            dgDatos.DataSource = dt;
            dgDatos.Refresh();

        }

        private void dgDatos_SelectionChanged(object sender, EventArgs e)
        {
            if(dgDatos.SelectedRows.Count > 0)
            {

            txtPersonID.Text = dgDatos.SelectedRows[0].Cells[0].Value.ToString();
            txtLastName.Text = dgDatos.SelectedRows[0].Cells[1].Value.ToString();
            txtFirstName.Text = dgDatos.SelectedRows[0].Cells[2].Value.ToString();
            txtHireDate.Text = dgDatos.SelectedRows[0].Cells[3].Value.ToString();
            txtEnrollmentDate.Text = dgDatos.SelectedRows[0].Cells[4].Value.ToString();

            }
        }

        private void btnListar_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt = np.GetAll();

            dgDatos.DataSource = dt;
            dgDatos.Refresh();

        }

        private void btnBuscarPorNombre_Click(object sender, EventArgs e)
        {

            String name = txtFirstName.Text;


            DataTable dt = new DataTable();
            dt = np.GetPersonByName(name);
            dgDatos.DataSource = dt;
            dgDatos.Refresh();

        }

        private void btnBuscarPorApellido_Click(object sender, EventArgs e)
        {
            String lastname = txtLastName.Text;


            DataTable dt = new DataTable();
            dt = np.GetPersonByLastName(lastname);
            dgDatos.DataSource = dt;
            dgDatos.Refresh();
        }
    }
}






